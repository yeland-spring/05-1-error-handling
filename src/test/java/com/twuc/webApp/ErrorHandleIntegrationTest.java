package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ErrorHandleIntegrationTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_handle_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/default", ErrorResponse.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertEquals("API-500", entity.getBody().getErrorCode());
        assertEquals("Something wrong with the argument", entity.getBody().getErrorMessage());
    }

    @Test
    void should_handle_another_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/exceptions/access-control-exception", ErrorResponse.class);
        assertEquals(HttpStatus.FORBIDDEN, entity.getStatusCode());
        assertEquals("API-403", entity.getBody().getErrorCode());
        assertEquals("access control", entity.getBody().getErrorMessage());
    }

    @Test
    void should_handle_multiple_exception_types_null_pointer() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/exceptions/null-pointer", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
    }

    @Test
    void should_handle_multiple_exception_types_arithmetic() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/exceptions/arithmetic", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody().getErrorMessage());
    }

    @Test
    void should_handle_exception_in_sister_controller() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody().getErrorMessage());
    }

    @Test
    void should_handle_exception_in_brother_controller() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody().getErrorMessage());
    }
}
