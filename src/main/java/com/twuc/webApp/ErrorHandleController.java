package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class ErrorHandleController {
    @GetMapping("/api/errors/default")
    String throwException() {
        throw new RuntimeException("Something wrong with the argument");
    }

    @ExceptionHandler(RuntimeException.class)
    ResponseEntity<ErrorResponse> handleException(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("API-500", exception.getMessage()));
    }

    @GetMapping("/api/exceptions/access-control-exception")
    String throwAnotherException() {
        throw new AccessControlException("access control");
    }

    @ExceptionHandler(AccessControlException.class)
    ResponseEntity<ErrorResponse> handleAccessException(AccessControlException exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ErrorResponse("API-403", exception.getMessage()));
    }

    @GetMapping("/api/exceptions/null-pointer")
    String throwNullPointerException(){
        throw new NullPointerException("Something wrong with the argument");
    }

    @GetMapping("/api/exceptions/arithmetic")
    String throwArithmeticException() {
        throw new ArithmeticException("Something wrong with the argument");
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    ResponseEntity<ErrorResponse> handleMultipleException(Exception exception) {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .body(new ErrorResponse("API-418", exception.getMessage()));
    }

}
