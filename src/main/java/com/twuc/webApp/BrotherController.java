package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BrotherController {
    @GetMapping("/api/brother-errors/illegal-argument")
    String brotherException() {
        throw new IllegalArgumentException("Something wrong with the argument");
    }
}
