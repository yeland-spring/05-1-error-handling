package com.twuc.webApp;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SisterController {
    @GetMapping("/api/sister-errors/illegal-argument")
    String sisterException() {
        throw new IllegalArgumentException("Something wrong with the argument");
    }
}
